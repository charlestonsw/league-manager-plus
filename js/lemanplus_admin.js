/* globals lmp_wp_data */
var LeManPlus = {
    app: {
	    vm: null,
	    data: lmp_wp_data,

	    /**
	     * Setup Vue Model
	     */
	    create_vm: function() {
		    this.vm = new Vue({
			    el: '#app' ,
			    data: this.data
		    });
	    },

    }
};

jQuery(document).ready( function() {
	LeManPlus.app.create_vm();
});
