<?php
/*
Plugin Name: League Manager Plus
Plugin URI: https://www.lance.bio/
Description: A simple league manager plugin.
Author: Lance Cleveland
Author URI: http://www.lance.bio
License: GPL3

Text Domain: league-manager-plus

Copyright 2018  Charleston Software Associates (info@storelocatorplus.com)

Tested up to: 4.9.4
Version: 0.1-beta-1
*/


// Die if not in WordPress version that supports add_action
//
if ( ! function_exists( 'add_action' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

//   Do not load this plugin on WP heartbeat since we don't do anything with it.
//
if ( defined( 'DOING_AJAX' ) && DOING_AJAX && ! empty( $_POST[ 'action' ] ) && ( $_POST[ 'action' ] === 'heartbeat' ) ) {
	return;
}

// Load the plugin.
function LEMAN_loader() {
	defined( 'LEMAN_FILE' ) || define( 'LEMAN_FILE', __FILE__ );
	defined( 'LEMAN_DIR'  ) || define( 'LEMAN_DIR' , dirname( __FILE__ ) );

	require_once( LEMAN_DIR . '/include/LeManPlus.php' );

	global $lemanplus;
	$lemanplus = new LeManPlus();
}
add_action( 'after_setup_theme' , 'LEMAN_loader' , 100 );