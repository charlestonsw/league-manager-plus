=== AISwarm ===
Plugin Name:  League Manager Plus
Contributors: charlestonsw
Donate link: http://lance.bio/
Tags: aiswarm, skynet, singularity, the matrix
License: GPL3
Required PHP: 5.3
Requires at least: 4.4
Tested up to: 4.9.4
Stable tag: 0.1-beta-1

A simple league manager plugin.

== Description ==

A basic WordPress app for setting up recreational league teams and tracking their win/loss record for a season.

= Requirements =

PHP 5.3+
WordPress 4.4+


== Changelog ==

= March 2018 =

Genesis.  Skynet's team coordinator is online.  Skynet Professional soon follow.

