<?php
defined( 'ABSPATH' ) || exit;

class LeManPlus_Admin_Page extends LeManPlus_Base_Object {
	private $plugin_dir;

	/**
	 * AISwarm_AdminPage constructor.
	 */
	public function initialize() {
		add_action( 'admin_enqueue_scripts', array( $this, 'give_me_some_style' ) );
		$this->plugin_dir = preg_replace( '/\/league-manager-plus(?:_\w+)?$/', '', LEMAN_DIR );
	}

	/**
	 * Add CSS.
	 */
	public function give_me_some_style( $hooker ) {
		global $lemanplus;
		if ( $hooker === $lemanplus->admin_page_hook ) {
			$this->nq_style( 'vuetify', '/css/vuetify.min.css' );
			//$this->nq_style( 'lemanplus_admin'   ,  '/css/lemanplus_admin.css'        );
			wp_enqueue_style( 'google-robot-material' , 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' );

			$this->register_script( 'what-input', '/js/what-input.js' );
			$this->register_script( 'vue', '/js/vue.min.js' );
			$this->register_script( 'vuetify', '/js/vuetify.min.js', array( 'vue' , 'what-input' ) );
			$this->register_script( 'lemanplus_admin', '/js/lemanplus_admin.js', array( 'vuetify' ) );


			wp_localize_script( 'lemanplus_admin', 'lmp_wp_data', array(
				'nonce' => wp_create_nonce( 'wp_rest' ),
			) );

			wp_enqueue_script( 'lemanplus_admin' );
		}
	}

	/**
	 * Enqueue a style in the CSS directory with timestamp checking.
	 *
	 * @param string $handle
	 * @param string $rel_file
	 */
	private function nq_style( $handle, $rel_file ) {
		wp_enqueue_style( $handle, plugins_url( $rel_file, LEMAN_FILE ), array(), filemtime( LEMAN_DIR . $rel_file ) );
	}

	/**
	 * Register a script in the js directory with timestamp checking.
	 *
	 * @param string $handle
	 * @param string $rel_file
	 */
	private function register_script( $handle, $rel_file, $deps = array() ) {
		wp_register_script( $handle, plugins_url( $rel_file, LEMAN_FILE ), $deps, filemtime( LEMAN_DIR . $rel_file ) );
	}

	/**
	 * Process form submissions.
	 */
	private function process_form_submissions() {
		if ( empty( $_POST['action'] ) ) {
			return;
		}
		if ( empty( $_POST['validate_lemanplus_action'] ) ) {
			return;
		}
		if ( ! wp_verify_nonce( $_POST['validate_lemanplus_action'], 'lemanplus_action' ) ) {
			return;
		}

		$method = 'process_form_' . sanitize_key( $_POST['action'] ) . '_action';
		if ( method_exists( $this, $method ) ) {
			$this->{$method}();
		}
	}

	/**
	 * Render the swarm control interface.
	 */
	public function render() {
		$this->process_form_submissions();
		?>
		<div id="app">
			<v-app>
				<h1>League Manager Plus</h1>

				<v-tabs icons-and-text centered dark color="cyan">
					<v-tabs-slider color="yellow"></v-tabs-slider>

					<v-tab href="#teams">
						Teams
						<v-icon>people</v-icon>
					</v-tab>

					<v-tab href="#about">
						About
						<v-icon>info</v-icon>
					</v-tab>

					<v-tab-item id="teams">
						<v-card flat>
							<v-card-text>Team card.</v-card-text>
						</v-card>
					</v-tab-item>

					<v-tab-item id="about">
						<v-card flat>
							<v-card-text>About card.</v-card-text>
						</v-card>
					</v-tab-item>

				</v-tabs>

			</v-app>
		</div>
		<?php
	}

}