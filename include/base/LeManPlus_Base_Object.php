<?php
defined( 'ABSPATH' ) || exit;

/**
 * Class LeManPlus_Base_Object
 */
class LeManPlus_Base_Object {

	/**
	 * LeManPlus_Base_Object constructor.
	 *
	 * @param array $options
	 */
	public function __construct( $options = array() ) {

		if ( is_array( $options ) && ! empty( $options ) ) {
			foreach ( $options as $property => $value ) {
				if ( property_exists( $this, $property ) ) {
					$this->$property = $value;
				}
			}
		}

		$this->initialize();
	}

	/**
	 * Return an instance of the object.
	 * @return mixed
	 */
	public static function get_instance() {
		static $instance;

		if ( ! isset( $instance ) ) {
			$class = get_called_class();
			$instance = new $class;
		}

		return $instance;
	}


	/**
	 * Do these things when this object is invoked.
	 */
	protected function initialize() {
		// Override with anything you want to run when your extension is invoked.
	}
}