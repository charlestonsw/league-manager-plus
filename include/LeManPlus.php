<?php
defined( 'ABSPATH' ) || exit;

/**
 * Class LeManPlus
 */
class LeManPlus {
	const icon = 'data:image/svg+xml;base64, PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MCA1MCI+PHBhdGggeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiBmaWxsPSIjOUFDRUU2IiBpZD0ic3ZnXzEiIGQ9Im04LDNjLTIuNzQ5NTIsMCAtNSwyLjI1MDQ4IC01LDVsMCwzNGMwLDIuNzQ5NTE5IDIuMjUwNDgsNSA1LDVsMzQsMGMyLjc0OTUxOSwwIDUsLTIuMjUwNDgxIDUsLTVsMCwtMzRjMCwtMi43NDk1MiAtMi4yNTA0ODEsLTUgLTUsLTVsLTM0LDB6bTAsMmwzNCwwYzEuNjY4NDgsMCAzLDEuMzMxNTIgMywzbDAsMzRjMCwxLjY2ODQ4IC0xLjMzMTUyLDMgLTMsM2wtMzQsMGMtMS42Njg0OCwwIC0zLC0xLjMzMTUyIC0zLC0zbDAsLTM0YzAsLTEuNjY4NDggMS4zMzE1MiwtMyAzLC0zem0wLDNsMCwzbDIsMGwwLC0zbC0yLDB6bTQsMGwwLDVsMiwwbDAsLTVsLTIsMHptNCwwbDAsMmwyLDBsMCwtMmwtMiwwem00LDBsMCw0bDIsMGwwLC00bC0yLDB6bTQsMGwwLDJsMiwwbDAsLTJsLTIsMHptNCwwbDAsM2wyLDBsMCwtM2wtMiwwem00LDBsMCwybDIsMGwwLC0ybC0yLDB6bTQsMGwwLDNsMiwwbDAsLTNsLTIsMHptNCwwbDAsNWwyLDBsMCwtNWwtMiwwem0tMjQsNGwwLDJsMiwwbDAsLTJsLTIsMHptOCwwbDAsMmwyLDBsMCwtMmwtMiwwem0tMTYsMWwwLDJsMiwwbDAsLTJsLTIsMHptMjAsMGwwLDJsMiwwbDAsLTJsLTIsMHptNCwwbDAsMmwyLDBsMCwtMmwtMiwwem00LDBsMCwybDIsMGwwLC0ybC0yLDB6bS0xNiwxbDAsMmwyLDBsMCwtMmwtMiwwem0tOCwxbDAsMmwyLDBsMCwtMmwtMiwwem0yOCwwbDAsMmwyLDBsMCwtMmwtMiwwem0tMTYsMWwwLDJsMiwwbDAsLTJsLTIsMHptLTE2LDFsMCwybDIsMGwwLC0ybC0yLDB6bTgsMGwwLDJsMiwwbDAsLTJsLTIsMHptMTIsMGwwLDJsMiwwbDAsLTJsLTIsMHptOCwwbDAsMmwyLDBsMCwtMmwtMiwwem0tMTYsMWwwLDJsMiwwbDAsLTJsLTIsMHptLTgsMWwwLDJsMiwwbDAsLTJsLTIsMHptMjgsMGwwLDJsMiwwbDAsLTJsLTIsMHptLTE2LDFsMCwybDIsMGwwLC0ybC0yLDB6bTgsMGwwLDJsMiwwbDAsLTJsLTIsMHptNCwxbDAsMmwyLDBsMCwtMmwtMiwwem0tMjgsMWwwLDJsMiwwbDAsLTJsLTIsMHptOCwwbDAsMmwyLDBsMCwtMmwtMiwwem00LDBsMCwybDIsMGwwLC0ybC0yLDB6bTgsMGwwLDJsMiwwbDAsLTJsLTIsMHptMTIsMWwwLDJsMiwwbDAsLTJsLTIsMHptLTI4LDFsMCwybDIsMGwwLC0ybC0yLDB6bTEyLDBsMCwybDIsMGwwLC0ybC0yLDB6bS00LDJsMCwybDIsMGwwLC0ybC0yLDB6bTE2LDBsMCwybDIsMGwwLC0ybC0yLDB6bS0yOCwxbDAsMmwyLDBsMCwtMmwtMiwwem04LDBsMCwybDIsMGwwLC0ybC0yLDB6bTEyLDBsMCwybDIsMGwwLC0ybC0yLDB6bTEyLDFsMCwybDIsMGwwLC0ybC0yLDB6bS0xNiwxbDAsMmwyLDBsMCwtMmwtMiwwem0tMTIsMWwwLDJsMiwwbDAsLTJsLTIsMHptOCwxbDAsMmwyLDBsMCwtMmwtMiwwem0xNiwxbDAsMmwyLDBsMCwtMmwtMiwwem0tMjgsMWwwLDJsMiwwbDAsLTJsLTIsMHptOCwwbDAsMmwyLDBsMCwtMmwtMiwwem0xMiwwbDAsMmwyLDBsMCwtMmwtMiwwem0tNCwxbDAsMmwyLDBsMCwtMmwtMiwwem0xNiwwbDAsMmwyLDBsMCwtMmwtMiwwem0tNCw0bDAsMmwyLDBsMCwtMmwtMiwwem0tMjgsMWwwLDJsMiwwbDAsLTJsLTIsMHptMTYsMGwwLDJsMiwwbDAsLTJsLTIsMHptLTgsMWwwLDJsMiwwbDAsLTJsLTIsMHptMjQsMGwwLDJsMiwwbDAsLTJsLTIsMHoiLz48L3N2Zz4=';
	public $admin_page_hook;

	public function __construct() {
		spl_autoload_register( array( $this, 'auto_load' ) );
		add_action( 'admin_menu', array( $this, 'create_menu' ) );
	}


	public function create_menu() {
		$this->admin_page_hook = add_menu_page( __( 'League Plus', 'league-manager-plus' ), __( 'League Plus', 'league-manager-plus' ) , 'install_plugins' , 'lemanplus_main', array( LeManPlus_Admin_Page::get_instance() , 'render' ), LeManPlus::icon , 10 );
	}

	/**
	 * Auto-loads classes of name AISwarm_<module>*  from include/<module> whenever it is needed.
	 *
	 * @param string $class_name
	 */
	public function auto_load( $class_name ) {
		if ( strpos( $class_name, 'LeManPlus_' ) === false ) {
			return;
		}

		$matches = array();
		preg_match( '/LeManPlus_(?<module>[a-zA-Z]*){1}(?<submodule>.*)+/', $class_name, $matches );

		$file_name = LEMAN_DIR . '/include/' . strtolower( ( ! empty( $matches['module'] ) ? $matches['module'] . '/' : '' ) ) . $class_name . '.php';
		if ( is_readable( $file_name ) ) {
			/** @noinspection PhpIncludeInspection */
			require_once( $file_name );
		}
	}

}