<?php
defined( 'ABSPATH' ) || exit;

/**
 * Class LeManPlus_VueTemplate
 */
class LeManPlus_VueTemplate extends LeManPlus_Base_Object {

	/**
	 * Go read a vue file from the /src/components directory.
	 *
	 * @param string $vue_file
	 */
	public function display_component( $vue_file ) {
		$fq_vue_file = LEMAN_DIR . '/src/components/'. $vue_file . '.vue';
		if ( is_readable( $fq_vue_file ) ) {
			/** @noinspection PhpIncludeInspection */
			include( $fq_vue_file );
		}
	}
}